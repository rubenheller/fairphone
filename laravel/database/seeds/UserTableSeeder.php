<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            [
                'name' => 'Ruben Heller',
                'email' => 'rubenheller@hotmail.nl',
                'password' => '$2y$12$QFgDH/lMft7DDy1nnhiX3.1pEGxj8SM24Uo1znL9ktYi81o1ByTBC'
            ],
        ]);
        factory(App\User::class, 230)->create();


    }
}
