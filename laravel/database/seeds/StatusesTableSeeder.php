<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->delete();

        DB::table('statuses')->insert([
            0 => [
                'id' => 1,
                'name' => 'Nieuw',
                'color' => '#cccc00',
            ],
            1 => [
                'id' => 2,
                'name' => 'Bezig',
                'color' => '#3399ff',
            ],
            2 => [
                'id' => 3,
                'name' => 'Klaar',
                'color' => '#009900',
            ],
        ]);
    }
}
