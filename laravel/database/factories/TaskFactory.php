<?php

use Faker\Generator as Faker;

$factory->define(\App\Task::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text(100),
        'status_id' => $faker->numberBetween(1,3)
    ];
});
