<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ public_path('favicon-16x16.png') }}">
    <title>{{ $title }}</title>
    {!! HTML::style('css/mdb.min.css') !!}
    {!! HTML::style('css/app.css') !!}
    {!! HTML::style('css/bootstrap.min.css') !!}
    <script src="//cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    @yield('style')
</head>
<body id="app">
@include('layouts.side_nav')
@include('layouts.upper_nav')
<main class="content-wrapper">
    @if(!$errors->isEmpty())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            @foreach($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @yield('content')
</main>
{!! HTML::script('js/app.js') !!}

{!! HTML::script('js/jquery-3.3.1.min.js') !!}
{!! HTML::script('js/popper.min.js') !!}
{!! HTML::script('js/mdb.min.js') !!}
{!! HTML::script('js/moment.js') !!}
{!! HTML::script('js/ez.countimer.min.js') !!}
{!! HTML::script('js/colorpicker.js') !!}
<script>
    CKEDITOR.replace( 'editor' );
    CKEDITOR.config.toolbar = [
        ['Styles','Format','Font','FontSize','Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
        ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        [,'Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
    ] ;
</script>
<script>
    $(".button-collapse").sideNav();
    $('.mdb-select').materialSelect();
    $('.clickable tr').click(function () {
        window.location.href = $(this).data('url');
    });
</script>
@if(Session::has('message'))
    <script>
        toastr.success('', '{{ Session::get('message') }}', {timeOut: 3000, progressBar: true});
    </script>
@endif
@yield('script')
</body>
</html>
