<div id="slide-out" class="side-nav fixed site-color">
    <ul>
        <li>
            <div class="logo-wrapper">
                <img src="{{ asset('images/logo.png') }}" class="img-fluid flex-center">
            </div>
        </li>
        <li>
            <ul class="collapsible collapsible-accordion">
                <li class="@if(Request::is('admin')) custom-active @endif">
                    <a href="{{ route('admin.home') }}" class="collapsible-header waves-effect">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li class="@if(Request::is('admin/task*')) custom-active @endif">
                    <a href="{{ route('admin.task.index') }}" class="collapsible-header waves-effect">
                        <i class="fas fa-tasks"></i>Taken</a>
                </li>
                <li class="@if(Request::is('admin/user*')) custom-active @endif">
                    <a href="{{ route('admin.user.index') }}" class="collapsible-header waves-effect">
                        <i class="fas fa-users"></i>Administrators</a>
                </li>
                <li class="@if(Request::is('admin/hour*')) custom-active @endif">
                    <a href="{{ route('admin.hour.index') }}" class="collapsible-header waves-effect">
                        <i class="fas fa-clock "></i>Uren</a>
                </li>
                <li class="@if(Request::is('admin/status*')) custom-active @endif">
                    <a href="{{ route('admin.status.index') }}" class="collapsible-header waves-effect">
                        <i class="fas fa-list"></i>Statussen</a>
                </li>
                {{--<li><a class="collapsible-header waves-effect arrow-r"><i class="fas fa-cog"></i> Instellingen<i class="fas fa-angle-down rotate-icon"></i></a>--}}
                    {{--<div class="collapsible-body">--}}
                        {{--<ul class="list-unstyled">--}}
                            {{--<li><a href="#" class="waves-effect">1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#" class="waves-effect">2</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </li>
        <!--/. Side navigation links -->
    </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>