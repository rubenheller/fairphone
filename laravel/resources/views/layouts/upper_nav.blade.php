<nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg site-color text-white">
    <!-- SideNav slide-out button -->
    <div class="float-left">
        <a href="#" style="color: white!important;" data-activates="slide-out" class="button-collapse black-text"><i class="fas fa-bars"></i></a>
    </div>

    <ul class="nav navbar-nav nav-flex-icons ml-auto">
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link waves-effect waves-light"><i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">{{ Auth::user()->name }}</span></a>--}}
        {{--</li>--}}
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false"><i class="fas fa-user"></i> {{ Auth::user()->name }}</a>
            <div class="dropdown-menu site-color" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item site-color-text waves-effect waves-light" href="{{route('admin.account', Auth::id()) }}">Account</a>
                <a class="dropdown-item site-color-text waves-effect" href="#"
                   onclick="$('#logout').submit()">Logout</a>
                {{ Form::open(['route' => 'logout', 'id' => 'logout']) }}
                {{Form::close()}}
            </div>
        </li>
    </ul>

</nav>