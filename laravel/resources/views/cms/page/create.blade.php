@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-title"><span class="head-title">{{ $title }}</span></div>
                </div>
            </div>
            {{ Form::open(['route' =>'admin.page.store']) }}
            <div class="md-form">
                <input name="title" type="text" id="titleBox" class="form-control">
                <label for="titleBox">Titel</label>
            </div>
            <button type="submit" class="btn btn-sm btn-custom pull-right">Toevoegen en Publiceren</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection