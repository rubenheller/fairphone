@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-title"><span class="head-title">{{ $title }}</span></div>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('admin.page.create') }}" class="btn btn-sm btn-custom pull-right">Pagina
                        toevoegen</a>
                </div>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th><b>Title</b></th>
                    <th><b>Url</b></th>
                    <th><b>Opties</b></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td width="40%">{{ $page->title }}</td>
                        <td width="40%">/{{ $page->url }}</td>
                        <td width="10%"><a href="{{ route('admin.page.edit', $page->id) }}" class="btn btn-sm btn-info">Pagina bekijken/bewerken</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection