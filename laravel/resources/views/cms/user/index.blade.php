@extends('layouts.app')
@section('content')
    <div class="card mb-5">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <span class="head-title">{{ $title }}</span>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-sm btn-custom pull-right">Nieuwe Administrator</a>
                    <a href="{{ route('admin.user.export') }}" class="btn btn-sm btn-primary pull-right">Download user overzicht</a>
                </div>
            </div>
        </div>
        <div class="card-body">

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th><b>Naam</b></th>
                    <th><b>Email</b></th>
                </tr>
                </thead>
                <tbody class="clickable">
                @foreach($users as $user)
                    <tr data-url="@if(Auth::id() == $user->id) {{ route('admin.account', $user->id)}}  @else {{ route('admin.user.edit', $user->id)}} @endif">
                        <td width="40%">{{ $user->name }} @if(Auth::id() == $user->id) <span class="text-danger">(Mijzelf)</span> @endif</td>
                        <td width="40%">{{ $user->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->render() }}
        </div>
    </div>
@endsection