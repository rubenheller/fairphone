@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-title"><span class="head-title">{{ $title }}</span></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{ Form::model($user,['route' =>['admin.user.update', $user->id], 'method' => 'PUT']) }}
                    {{ Form::hidden('user_id', $user->id) }}
                    @include('cms.user.form')
                </div>
            </div>
            <button type="submit" class="btn btn-sm btn-custom pull-right">Bewerken</button>
            <button type="button" class="btn btn-sm btn-danger pull-right" data-toggle="modal" data-target="#deleteModal">Administrator verwijderen</button>
            {{ Form::close() }}
            {{ Form::open(['route' => ['admin.user.destroy' ,$user->id], 'method' => 'DELETE', 'id' => 'delete']) }}
            {{ Form::close() }}
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Weet u zeker dat u {{ $user->name }} wil verwijderen?</h5>
                        </div>
                        <div class="modal-body">
                            <p><span class="text-danger">Waarschuwing!</span><br>Als u deze user verwijderd, worden ook alle uren van deze user verwijderd!</p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-sm btn-danger" data-dismiss="modal">Nee</a>
                            <a class="btn btn-sm btn-success" onclick="$('#delete').submit()">Ja</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection