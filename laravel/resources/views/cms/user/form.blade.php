<div class="md-form">
    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id'=> 'nameBox']) }}
    <label for="nameBox">Naam</label>
</div>
<div class="md-form md-outline">
    {{ Form::email('email',  old('email'), ['class' => 'form-control', 'id'=> 'emailBox']) }}
    <label for="emailBox">Email</label>
</div>
<div class="md-form">
    <input type="password" name="password" id="passwordBox" class="form-control">
    <label for="passwordBox">Wachtwoord</label>
</div>
<div class="md-form">
    <input type="password" name="password_confirmation" id="password_conBox" class="form-control" >
    <label for="password_conBox">Wachtwoord bevestigen</label>
</div>