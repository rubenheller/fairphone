@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ $title }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    {{ Form::open(['route' =>'admin.user.store']) }}
                    @include('cms.user.form')
                </div>
            </div>
            <button type="submit" class="btn btn-sm btn-custom pull-right">Toevoegen</button>
            {{ Form::close() }}

        </div>
    </div>
@endsection