@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-header">
                    {{ $title }}
                </div>
                <div class="card-body">
                    {{ Form::open(['route' =>'admin.task.store']) }}
                    @include('cms.task.form')
                    <button type="submit" class="btn btn-sm btn-custom pull-right mt-4">Toevoegen</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Uren Registratie Huidige Taak
                </div>
                <div class="card-body text-center">
                    Maak eerst de taak aan voordat de urenregistratie gestart kan worden
                </div>
            </div>
        </div>
    </div>
@endsection