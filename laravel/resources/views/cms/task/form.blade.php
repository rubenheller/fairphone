<div class="md-form">
    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id'=> 'nameBox']) }}
    <label for="nameBox">Naam</label>
</div>

<span style="color: #757575">Status</span>
{{ Form::select('status_id', $statuses,null,['class'=>'mdb-select md-form mt-0','placeholder' => 'Selecteer een status']) }}

<span style="color: #757575;">Omschrijving</span>
<textarea placeholder="test" name="description" id="editor">
        @if(isset($task)) {!! $task->description !!} @else {!! old('description') !!} @endif
</textarea>