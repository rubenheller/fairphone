@extends('layouts.app')
@section('content')
    <div class="card mb-5">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <span class="head-title">{{ $title }}</span>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('admin.task.create') }}" class="btn btn-sm btn-custom pull-right">Nieuwe Taak</a>
                    <a href="#" class="btn btn-sm btn-custom pull-right"><i class="fas fa-search"></i></a>
                    <div class="md-form form-sm d-inline-block m-0 pull-right">
                        <input name="search" type="text" id="inputSMEx" class="form-control form-control-sm">
                        <label for="inputSMEx">Taak zoeken</label>
                    </div>
                    {{ Form::open(['route' => 'admin.task.index']) }}
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover table-responsive">
                <thead>
                <tr class="text-center">
                    <th><b>Naam</b></th>
                    <th><b>Omschrijving</b></th>
                    <th><b>Mijn Gewerkte Tijd</b></th>
                    <th><b>Status</b></th>
                    <th><b>Opties</b></th>
                </tr>
                </thead>
                <tbody>
                @forelse($tasks as $task)
                    <tr class="text-center  ">
                        <td width="20%">{{ $task->name }}</td>
                        <td width="45%"><a href="#" class="show-description text-primary"
                                           id="{{$task->id}}">Weergeven</a>
                            <div id="taskDescription_{{$task->id}}" style="display: none;">
                                <hr>
                                {!! $task->description !!}
                                <hr>
                            </div>
                        </td>
                        <td width="20%" style="padding-top: 22px;">
                            @if($task->users->where('id', Auth::id())->first())
                                <span>
                                    {{ gmdate('H:i:s', $task->users->where('id', Auth::id())->first()->pivot->seconds) }}
                                </span>
                            @endif
                        </td>
                        <td width="25%">
                            <span class="badge w-100 h-100"
                                  style="background-color: {{ $task->status->color }};padding: .7rem 1.6rem;">{{ $task->status->name }}</span>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-info w-100" href="{{ route('admin.task.edit', $task->id) }}">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr data-url="{{ route('admin.task.create')}}">
                        <td colspan="2">Nog geen taken aangemaakt.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $tasks->render() }}
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.show-description').click(function () {
            var id = $(this).attr('id');
            if ($(this).text() === 'Weergeven') {
                $(this).text('Verbergen');
            } else {
                $(this).text('Weergeven');
            }
            $('#taskDescription_' + id).toggle();
        });
    </script>
@endsection