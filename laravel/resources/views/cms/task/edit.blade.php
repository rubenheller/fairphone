@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    {{ $title }}
                </div>
                <div class="card-body">
                    {{ Form::model($task,['route' =>['admin.task.update', $task->id], 'method' => 'PUT']) }}
                    @include('cms.task.form')
                    <button type="submit" class="btn btn-sm btn-custom pull-right mt-4">Bewerken</button>
                    <button type="button" class="btn btn-sm btn-danger pull-right mt-4" data-toggle="modal"
                            data-target="#deleteModal">Taak verwijderen
                    </button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card h-100">
                <div class="card-header">
                    Uren Registratie Huidige Taak
                </div>
                <div class="card-body text-center">
                    <div style="font-size: 5.3rem;padding-top: 100px;" class="timer"></div>
                    <button type="button" class="btn btn-lg btn-primary w-50 start-time"
                            onclick="$('.timer').countimer('resume');">Start
                    </button>
                    <button type="button" style="display: none" class="btn btn-lg btn-warning w-50 stop-time"
                            onclick="$('.timer').countimer('stop');">Stop
                    </button>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card h-100 mt-xl-5 mb-xl-5">
                <div class="card-header">
                    Gebruikers die gewerkt hebben aan deze taak
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th><b>Naam</b></th>
                            <th><b>Gewerkte Tijd</b></th>
                            <th><b>Voor het laatst aan gewerkt</b></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td width="10%">{{ $user->name }}@if(Auth::id() == $user->id) <span class="text-danger">(Mijzelf)</span> @endif</td>
                                <td width="10%">{{ gmdate("H:i:s", $user->pivot->seconds) }}</td>
                                <td width="10%">{{ $user->pivot->updated_at->format('d-m-Y H:i') }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Niemand heeft nog gewerkt aan deze taak</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
    {{ Form::open(['route' => ['admin.task.destroy' ,$task->id], 'method' => 'DELETE', 'id' => 'delete']) }}
    {{ Form::close() }}
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Weet u zeker dat u taak {{ $task->number }} wil
                        verwijderen?</h5>
                </div>
                <div class="modal-body">
                    <p><span class="text-danger">Waarschuwing!</span><br>Als u deze taak verwijderd, worden ook alle geregistreerde uren van deze taak verwijderd!</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-sm btn-success" onclick="$('#delete').submit()">Ja</a>
                    <a class="btn btn-sm btn-danger" data-dismiss="modal">Nee</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('.timer').countimer({
            autoStart: false,
            initSeconds: {{ $seconds }}
        });
        $('.start-time').click(function () {
            $(this).hide();
            $('.stop-time').show();
        });
        $('.stop-time').click(function () {
            $(this).hide();
            $('.start-time').show();
            var time = $('.timer').text();
            var url = "{{ route('admin.task.save-time', ['id' => $task->id,'time'=> 'time']) }}";
            var _token = $("input[name='_token']").val();
            var task = "{{ $task->id }}";
            url = url.replace('time', time);
            $.ajax({
                type: "POST",
                url: url,
                data: {_token: _token, task: task, time: time},
                beforeSend: function () {
                    $('.stop-time').attr('disabled', true);
                },
                success: function (data) {
                    toastr.success('', 'Tijd opgeslagen', {timeOut: 3000, progressBar: true});
                    setTimeout(function () {
                        $('.stop-time').attr('disabled', false);
                    }, 2000);
                }
            });
        });
    </script>
@endsection