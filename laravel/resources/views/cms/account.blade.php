@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ $title }}</div>
                <div class="card-body">
                    {{ Form::model($user, ['route' => ['admin.account.update', $user->id], 'method' => 'PUT']) }}
                    <div class="md-form">
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'id'=> 'nameBox']) }}
                        <label for="nameBox">Naam</label>
                    </div>
                    <div class="md-form">
                        {{ Form::email('email',  old('email'), ['class' => 'form-control', 'id'=> 'emailBox']) }}
                        <label for="emailBox">Email</label>
                    </div>
                    {{--<div class="md-form">--}}
                        {{--<input type="password" name="password" id="passwordBox" class="form-control">--}}
                        {{--<label for="passwordBox">Wachtwoord</label>--}}
                    {{--</div>--}}
                    {{--<div class="md-form">--}}
                        {{--<input type="password" name="password_confirmation" id="password_conBox" class="form-control" >--}}
                        {{--<label for="password_conBox">Wachtwoord bevestigen</label>--}}
                    {{--</div>--}}
                    <button type="submit" class="btn btn-sm btn-custom pull-right mt-4">Wjzigingen opslaan</button>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection