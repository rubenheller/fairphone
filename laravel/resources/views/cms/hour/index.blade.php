@extends('layouts.app')
@section('content')
    <div class="card mb-5">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <span class="head-title">{{ $title }}</span>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('admin.hour.export') }}" class="btn btn-sm btn-primary pull-right">Download Uren Overzicht</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr class="text-center">
                    <th><b>Naam</b></th>
                    <th><b>Taak + Gewerkte tijd</b></th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    @if(!$user->tasks->isEmpty())

                        <tr>
                            <td>{{ $user->name }} @if(Auth::id() == $user->id)<span class="text-danger">(Mijzelf)</span>@endif </td>
                            <td>
                                <div class="row">
                                    @foreach($user->tasks as $task)
                                        <div class="col-md-3">
                                            <a href="{{ route('admin.task.edit', $task->id) }}"
                                               style="text-decoration: none">
                                                <div class="card m-2 waves-effect">
                                                    <div class="card-header"
                                                         style="background-color: {{$task->status->color}}">
                                                        <span style="white-space: nowrap;font-weight: 600">{{ $task->name }}</span><br>
                                                        <span class="badge">{{$task->status->name}}</span>
                                                    </div>
                                                    <div class="card-body">
                                                        {{ gmdate("H:i:s", $task->pivot->seconds) }}
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endif
                @empty
                    <tr data-url="{{ route('admin.task.create')}}">
                        <td colspan="2">Geen urenregistratie</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $users->render() }}--}}
        </div>
    </div>
@endsection