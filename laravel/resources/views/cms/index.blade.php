@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Aantal Administators
                </div>
                <div class="card-body">
                    <div class="count-box">
                        {{ $total_users_count }}
                    </div>
                </div>
            </div>
            <div class="card mt-4 mb-4">
                <div class="card-header">
                    Totaal aantal uren gewerkt door {{ Auth::user()->name }}
                </div>
                <div class="card-body">
                    <div class="count-box">
                        {{gmdate('H:i:s', $total_hours_count) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card" style="padding-bottom: 14px;">
                <div class="card-header">
                    Aantal Taken
                </div>
                <div class="card-body">
                    <div class="count-box" style="font-size: 3.5rem;">
                        {{ $total_tasks_count }} Totaal<br>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><b>Status</b></th>
                                <th><b>Aantal Taken</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($statuses as $status)
                                <tr>
                                    <td width="40%">{{ $status->name }}</td>
                                    <td width="40%">{{ $status->tasks->count() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        .count-box {
            padding: 20px;
            font-size: 5rem;
            text-align: center;
        }
    </style>
@endsection