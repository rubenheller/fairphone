@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ $title }}
        </div>
        <div class="card-body">
            {{ Form::open(['route' =>'admin.status.store']) }}
            @include('cms.status.form')
            <button type="submit" class="btn btn-sm btn-custom pull-right mt-4">Toevoegen</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection