@extends('layouts.app')
@section('content')
    <div class="card mb-5">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <span class="head-title">{{ $title }}</span>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('admin.status.create') }}" class="btn btn-sm btn-custom pull-right">Nieuwe Status</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th><b>Kleur</b></th>
                    <th><b>Naam</b></th>
                </tr>
                </thead>
                <tbody class="clickable">
                @forelse($statuses as $status)
                    <tr data-url="{{ route('admin.status.edit', $status->id) }}">
                        <td width="10%" style="background-color: {{ $status->color }}">&nbsp;</td>
                        <td width="80%">{{ $status->name }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">Geen statusen, zorg dat de seeder uitgevoerd is!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{--{{ $statuss->render() }}--}}
        </div>
    </div>
@endsection