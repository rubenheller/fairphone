<div class="md-form">
    @if(isset($status))
        {{ Form::text('name', $status->name, ['class' => 'form-control', 'id'=> 'nameBox']) }}
    @else
        {{ Form::text('name', null, ['class' => 'form-control', 'id'=> 'nameBox']) }}
    @endif
    <label for="nameBox">Naam</label>
</div>
<label style="color: #757575" for="editor">Kleur</label>
@if(isset($status))
    {{ Form::color('color', $status->color, ['class' => 'form-control w-25', 'id'=> 'colorBox']) }}
@else
    {{ Form::color('color', old('color'), ['class' => 'form-control w-25', 'id'=> 'colorBox']) }}
@endif
