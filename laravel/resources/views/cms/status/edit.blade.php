@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ $title }}
        </div>
        <div class="card-body">
            {{ Form::model($status,['route' =>['admin.status.update', $status->id], 'method' => 'PUT']) }}
            @include('cms.status.form')
            <button type="submit" class="btn btn-sm btn-custom pull-right mt-4">Bewerken</button>
            @if(!in_array($status->id, [1, 2, 3]))
            <button type="button" class="btn btn-sm btn-danger pull-right mt-4" data-toggle="modal"
                    data-target="#deleteModal">Status verwijderen
            </button>
            @endif
            {{ Form::close() }}
        </div>
    </div>

    {{ Form::close() }}
    {{ Form::open(['route' => ['admin.status.destroy' ,$status->id], 'method' => 'DELETE', 'id' => 'delete']) }}
    {{ Form::close() }}
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Weet u zeker dat u {{ $status->name }} wil
                        verwijderen?</h5>
                </div>
                <div class="modal-body">
                    <p><span class="text-danger">Waarschuwing!</span><br>Als u deze status verwijderd, worden ook alle
                        geregistreerde uren van deze taak verwijderd!</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-sm btn-danger" data-dismiss="modal">Nee</a>
                    <a class="btn btn-sm btn-success" onclick="$('#delete').submit()">Ja</a>
                </div>
            </div>
        </div>
    </div>

@endsection