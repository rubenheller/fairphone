<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/account/{id}', 'HomeController@accountSettings')->name('account');
Route::put('/account/{id}', 'HomeController@saveAccountSettings')->name('account.update');


Route::resource('page', 'PageController');

Route::get('user/export', 'UserController@export')->name('user.export');
Route::resource('user', 'UserController');

Route::resource('task', 'TaskController');
Route::post('task/{id}/{time}', 'TaskController@saveTime')->name('task.save-time');

Route::get('hour/export', 'HourController@export')->name('hour.export');

Route::resource('hour', 'HourController');
Route::resource('status', 'StatusController');



