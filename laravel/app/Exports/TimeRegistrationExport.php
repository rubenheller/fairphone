<?php

namespace App\Exports;

use App\Task;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Excel;

class TimeRegistrationExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        ini_set('memory_limit', '500m');
        $users = User::all();
        $user_collection = [];
        foreach ($users as $user) {
            $user_collection[$user->id] = new \stdClass();
            $user_collection[$user->id]->name = $user->name;
            $user_collection[$user->id]->tasks = [];
            if (!$user->tasks->isEmpty()) {
                foreach ($user->tasks as $task) {
                    $user_collection[$user->id]->tasks[$task->name] = gmdate('H:i:s', $task->pivot->seconds);
                }
            }
        }
        return collect($user_collection);
    }
}
