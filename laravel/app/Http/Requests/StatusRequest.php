<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StatusRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'color' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Naam is verplicht',
            'color.required' => 'Kleur is verplicht'
        ];
    }

}
