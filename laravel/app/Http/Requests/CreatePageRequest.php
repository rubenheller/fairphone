<?php

namespace App\Http\Requests;

use App\Page;
use Illuminate\Foundation\Http\FormRequest;

class CreatePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->request->title = Page::titleToUrl($this->request->get('title'));
        return [
            'title' => 'unique:pages,title'
        ];
    }
    public function messages()
    {
        return [
            'title.unique' => 'Pagina bestaat al.'
        ];
    }
}
