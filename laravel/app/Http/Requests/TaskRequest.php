<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'status_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Naam is verplicht',
            'description.required' => 'Een beschrijving is verplicht',
            'status_id.required' => 'Selecteer een status',
        ];
    }
}
