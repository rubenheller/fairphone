<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Task extends Model
{
    protected $fillable = ['name', 'description', 'status_id'];

    public static function convertCreatedAt($updated_at)
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at, 'UTC');
        $date->setTimezone('Europe/Amsterdam');
        return $date;
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('seconds')->withTimestamps();
    }
    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
