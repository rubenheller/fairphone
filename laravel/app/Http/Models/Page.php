<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title', 'url'];

    public static function titleToUrl($title)
    {
        $lower_title = strtolower($title);
        $url = str_replace(' ', '-', $lower_title);
        return trim($url);
    }
}
