<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
  public function getView()
  {
      $view = view('pages.show');
      $view->page = Page::where('url', request()->route()->uri())->first();
      return $view;
  }
}
