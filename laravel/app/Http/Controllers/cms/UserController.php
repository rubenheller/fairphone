<?php namespace App\Http\Controllers\Admin;

use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index()
    {
        $view = view('cms.user.index');
        $view->title = 'Administrators';
        $view->users = User::paginate(10);
        return $view;
    }

    public function create()
    {
        $view = view('cms.user.create');
        $view->title = 'Administrator toevoegen';
        return $view;
    }

    public function store(UserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        User::create($input);
        return redirect()->route('admin.user.index')->withMessage('Administrator aangemaakt');
    }


    public function edit(User $user)
    {
       $view = view('cms.user.edit');
       $view->user = $user;
       $view->title = 'Administrator bewerken';
       return $view;
    }

    public function update(UserRequest $request, User $user)
    {
        $input = $request->except('user_id');
        $input['password'] = Hash::make($input['password']);
        $user->update($input);
        return redirect()->route('admin.user.edit', $user->id)->withMessage('Administrator is bijgewerkt');
    }

    public function destroy(User $user)
    {
        $name = $user->name;
        $user->tasks()->wherePivot('user_id','=',$user->id)->detach();
        $user->delete();
        return redirect()->route('admin.user.index')->withMessage('Administrator '.$name.' is verwijderd');
    }
    public function export()
    {
        return Excel::download(new UserExport, 'users_'.date('m_d_y').'.xlsx');
    }
}
