<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePageRequest;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function index()
    {
        $view = view('cms.page.index');
        $view->pages = Page::all();
        $view->title = "Pagina's";

        return $view;
    }


    public function create()
    {
        $view = view('cms.page.create');
        $view->title = 'Pagina toevoegen';
        return $view;
    }


    public function store(CreatePageRequest $request)
    {
       $page = Page::create([
           'title' => $request->title,
           'url' => Page::titleToUrl($request->title)
       ]);
       return redirect()->route('admin.page.edit', $page->id)->withMessage('Pagina aangemaakt en gepubliceert');
    }

    public function show(Page $page)
    {
        dd($page);
    }

    public function edit(Page $page)
    {
        $view = view('cms.page.edit');
        $view->title = $page->title;
        $view->page = $page;

        return $view;
    }

    public function update(Request $request, Page $page)
    {
        //
    }


    public function destroy(Page $page)
    {
        //
    }
}
