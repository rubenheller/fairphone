<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Status;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    public function index()
    {
        $view = view('cms.task.index');
        $view->title = 'Taken';
        $view->tasks = Task::orderBy('created_at', 'DESC')->with('status', 'users')->paginate(12);
        return $view;
    }

    public function create()
    {
        $view = view('cms.task.create');
        $view->title = 'Taak Toevoegen';
        $view->statuses = Status::pluck('name', 'id');
        return $view;
    }

    public function store(TaskRequest $request)
    {
        $task = Task::create($request->all());
        $task->users()->attach(Auth::id(),['seconds' => 0]);
        return redirect()->route('admin.task.edit', $task->id)->withMessage('Taak aangemaakt');
    }

    public function edit(Task $task)
    {
        $view = view('cms.task.edit');
        $view->task = $task;
        $view->title = 'Taak bewerken';
        $view->statuses = Status::pluck('name', 'id');
        $worked_users = [];
        foreach ($task->users as $user){
            if($user->pivot->seconds > 0){
                $user->pivot->updated_at = Task::convertCreatedAt($user->pivot->updated_at);
                $worked_users[] = $user;
            }
        }
        $view->users = $worked_users;
        if(Auth::user()->tasks->find($task->id) != null){
            $view->seconds = Auth::user()->tasks->find($task->id)->pivot->seconds;
        }else{
            $view->seconds = 0;
        }
        return $view;
    }

    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->all());
        return redirect()->route('admin.task.edit', $task->id)->withMessage('Taak is bijgewerkt');
    }


    public function destroy(Task $task)
    {
        $task->users()->wherePivot('task_id','=',$task->id)->detach();
        $task->delete();
        return redirect()->route('admin.task.index')->withMessage('Taak verwijderd');
    }
    public function saveTime($id, $time)
    {
        $task = Task::find($id);
        $seconds = strtotime("1970-01-01 $time UTC");
        Auth::user()->tasks()->detach($task->id, ['seconds' => $seconds]);
        Auth::user()->tasks()->attach($task->id, ['seconds' => $seconds]);
        return response('OK');
    }
}
