<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StatusRequest;
use App\Status;

class StatusController extends Controller
{
    public function index()
    {
        $view = view('cms.status.index');
        $view->title = 'Statussen';
        $view->statuses = Status::all();
        return $view;
    }

    public function create()
    {
        $view = view('cms.status.create');
        $view->title = 'Nieuwe Status';

        return $view;
    }

    public function store(StatusRequest $request)
    {
        $status = Status::create($request->all());
        return redirect()->route('admin.status.edit', $status->id)->withMessage('Nieuwe status: '. $status->name. ' aangemaakt');

    }

    public function edit(Status $status)
    {
        $view = view('cms.status.edit');
        $view->title = 'Status bewerken';
        $view->status = $status;

        return $view;
    }

    public function update(StatusRequest $request, Status $status)
    {
        $status->update($request->all());
        return redirect()->route('admin.status.edit', $status->id)->withMessage('Status is bijgewerkt');
    }

    public function destroy(Status $status)
    {
        dd('todo');
    }
}
