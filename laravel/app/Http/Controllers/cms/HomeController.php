<?php namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Status;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index()
    {
        $view = view('cms.index');
        $view->title = 'Dashboard';
        $view->total_users = User::with('tasks', 'tasks.status')->get();
        $view->total_users_count = $view->total_users->count();
        $view->total_tasks_count = Task::all()->count();
        $view->statuses = Status::with('tasks')->get();
        $total_hours_count = [];
        foreach ($view->total_users as $user) {
            foreach ($user->tasks as $task) {
                $task = Auth::user()->tasks->find($task->id);
                if(isset($task->pivot)) {
                    $total_hours_count[] = $task->pivot->seconds;
                }
            }
        }
        $view->total_hours_count = collect($total_hours_count)->sum();

        return $view;
    }
    public function accountSettings($id)
    {
        $view = view('cms.account');
        $view->title = 'Mijn Account';
        $view->user = User::find($id);
        return $view;
    }
    public function saveAccountSettings(Request $request, $id)
    {
       $user = User::find($id);
       $user->update($request->all());
       return redirect()->back()->withMessage('Veranderingen opgeslagen');

    }
}
