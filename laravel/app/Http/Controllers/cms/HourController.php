<?php namespace App\Http\Controllers\Admin;


use App\Exports\TimeRegistrationExport;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HourController extends Controller
{
    public function index()
    {
        $view = view('cms.hour.index');
        $view->title = 'Uren';
        $view->users = User::with(['tasks', 'tasks.status'])->get();
        return $view;
    }
    public function export()
    {
        return Excel::download(new TimeRegistrationExport, 'time_registarion_'.date('m_d_y').'.xlsx');
    }
}
